const API_ROUTES = {
    getUsers   : { url: '?action=users', method: 'get'},
    getUser    : { url: '?action=get-user&id={user_id}', method: 'get'},
    addUser    : { url: '?action=add-user', method: 'post'},
    updateUser : { url: '?action=update-user&id={id}', method: 'post'},
    deleteUser : { url: '?action=delete-user&id={id}', method: 'get'},
    login      : { url: '?action=login', method: 'post'},
}

export default API_ROUTES;