import axios from 'axios';

const API_URL = 'http://front-end/api/store.php';
const JWT_TOKEN_NAME = 'X-JWT-USER-TOKEN';

const getToken = () => {
    let token = localStorage.getItem(JWT_TOKEN_NAME);
    return (token) ? token : '';
}

const setToken = (token) => {
    localStorage.setItem(JWT_TOKEN_NAME, token);
}

const removeToken = () => {
    localStorage.removeItem(JWT_TOKEN_NAME);
}

const HEADER_LIST = {
    "Content-Type": "application/json",
}

HEADER_LIST[JWT_TOKEN_NAME] = getToken()

const Api = axios.create({
    baseURL: API_URL,
    headers: HEADER_LIST,
});

///////////////////////////
Api.interceptors.request.use(
    (config) => {
        return config;
    },

    (error) => {
        return Promise.reject(error);
    }
);

//////////////////////////
Api.interceptors.response.use(

    (result) => {
        if(result.data) {
             const data = result.data;
             if(data.debug) {
                 console.log('DEBUG', data.debug);
             }
        }
        return result.data;
    },

    async (error) => {
        alert('HTTP ERROR');
        console.log('HTTP ERROR', error);
        return Promise.reject(error);
    }
);

////////////////////////////
const send = (url, method = 'get', data = null) => {
    if(data) return Api[method](url, data);
    else     return Api[method](url);
}

export default {
    tokenName : JWT_TOKEN_NAME,
    api: Api,
    send,
    setToken,
    getToken,
    removeToken,
};