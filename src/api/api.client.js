import apiRequest from '@/api/api.send.js';
import API_ROUTES from '@/api/api.routes.js';

const urlFormatter = (url, params) => {
    let realUrl = url;
    for (let i in params) {
        let value = params[i];
        let start = realUrl.indexOf("{");
        let end = realUrl.indexOf("}");
        let pattern = realUrl.slice(start, end + 1);
        realUrl = realUrl.replace(pattern, value);
    }
    return realUrl;
}

const responseHandle = (data, callbacks) => {
    let resolve = (callbacks[0]) ? callbacks[0] : null;
    let reject = (callbacks[1]) ? callbacks[1] : null;
    if (reject && data.error)
        return reject(data.error);
    return resolve(data.result)
}

const make = async (args, data = null, callbacks = null) => {

    let routeName = (args[0]) ? args[0] : '';
    let urlParams = (args[1]) ? args[1] : null;

    if (!API_ROUTES[routeName]) {
        alert('Такой роут не найден:' + routeName);
        return false;
    }

    const route = API_ROUTES[routeName];
    let method = (route['method']) ? route['method'] : 'get';
    let url = route['url'];
    if (urlParams) {
        url = urlFormatter(url, urlParams)
    }

    let resultData = await apiRequest.send(url, method, data);
    if (callbacks) {
        resultData = responseHandle(resultData, callbacks)
    }
    return resultData;
}

export default {
    routes: API_ROUTES,
    tokenName: apiRequest.tokenName,
    send: apiRequest.send,
    setToken: apiRequest.setToken,
    getToken: apiRequest.getToken,
    removeToken: apiRequest.removeToken,
    responseHandle,
    make,
};