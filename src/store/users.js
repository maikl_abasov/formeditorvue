import { defineStore } from 'pinia';
import useMessageShow from "@/uses/useMessageShow.js";
import ApiClient from '@/api/api.client.js';
import useLocalStorage from '@/uses/useLocalStorage';
const localStore = useLocalStorage()
const messageShow = useMessageShow()

export const useUserStore = defineStore('users', {

    state: () => {
        return {
            users: [],
            user : {},
            currentUser: {},
            message: {},
        }
    },

    actions: {

        getUsers() {
            ApiClient.make(['getUsers']).then(response => {
                this.users = response.users;
            })
        },

        getUser(id) {
            ApiClient.make(['getUser', [id]]).then(response => {
                 this.user = response.user;
            })
        },

        addUser(data) {
            ApiClient.make(['addUser'], data).then(response => {
                if(response.save)  {
                  this.alertMessage('Новый пользователь добавлен');
                  this.getUsers();
                } else  {
                  this.alertMessage('Не удалось добавить пользователя', '', 'error');
                }
            })
        },

        updateUser(data, id = 0) {
            ApiClient.make(['updateUser', [id]], data).then(response => {
                if(response.save)  {
                    this.alertMessage('Данные изменены');
                    this.getUsers();
                } else  {
                    this.alertMessage('Не удалось изменить данные', '', 'error');
                }
            })
        },

        deleteUser(id) {
            ApiClient.make(['deleteUser', [id]]).then(response => {
                if(response.save)  {
                    this.alertMessage('Данные изменены');
                } else  {
                    this.alertMessage('Не удалось изменить данные', '', 'error');
                }
            })

        },

        login(data) {

            ApiClient.make(['login'], data).then(response => {
                if(response.user && response.user.name) {
                    this.currentUser = response.user;
                    localStore.set('current_user', this.currentUser, true);
                    this.alertMessage('Авторизация прошла успешно');
                    return true;
                }
                this.alertMessage('Не удалось найти пользователя', '', 'error');
            })
        },

        logout() {
            this.currentUser = {};
            localStore.remove('current_user');
        },

        setCurrentUser() {
            this.currentUser = localStore.get('current_user', true);
        },

        alertMessage(title, text = '', type = "success", duration = 3000) {
            messageShow.show(title, text, type, duration);
        },
    },
})